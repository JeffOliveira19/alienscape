//
//  GameScene.swift
//  AlienEscape
//
//  Created by Jefferson de Oliveira Lalor on 25/10/18.
//  Copyright © 2018 Lalor Projects. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var playerNode: SKSpriteNode!
    
    var enemy1Node: SKSpriteNode!
    var enemy1Velocity: CGPoint!
    var explosionSound: SKAction!
    
    var gameOverNode: SKLabelNode!
    var isGameOver = false
    
    var scoreNode: SKLabelNode!
    
    var coinNode: SKSpriteNode!
    var nCoins = 0
    var coinSound: SKAction!
    
    var coinHUD: SKSpriteNode!
    var coinIdleFrames: [SKTexture]!
    var coinFlipFrames: [SKTexture]!
    
    
    var lastUpdateTime = CFTimeInterval()
    
    override func didMove(to view: SKView) {
        
        coinHUD = (self.childNode(withName: "CoinHUD") as! SKSpriteNode)
        
        coinIdleFrames = [ SKTexture(imageNamed: "gold_1"),
                           SKTexture(imageNamed: "gold_2"),
                           SKTexture(imageNamed: "gold_3"),
                           SKTexture(imageNamed: "gold_4"),
                           SKTexture(imageNamed: "gold_5"),
                           SKTexture(imageNamed: "gold_6"),
                           SKTexture(imageNamed: "gold_1"),]
        
        coinFlipFrames = [ SKTexture(imageNamed: "gold_1"),
                           SKTexture(imageNamed: "gold_2"),
                           SKTexture(imageNamed: "gold_3"),
                           SKTexture(imageNamed: "gold_4"),
                           SKTexture(imageNamed: "gold_5"),
                           SKTexture(imageNamed: "gold_6"),
                           SKTexture(imageNamed: "gold_1"), ]
        
        let idleAnimation = SKAction.animate(with: coinIdleFrames, timePerFrame: 0.3)
        let loopIdAnimation = SKAction.repeatForever(idleAnimation)
        coinHUD.run(loopIdAnimation)
        
        playerNode = (self.childNode(withName: "Player") as! SKSpriteNode)
        
        enemy1Node = (self.childNode(withName: "Enemy1") as! SKSpriteNode)
        enemy1Velocity = CGPoint(x: 300.0, y: 300.0)
        
        gameOverNode = (self.childNode(withName: "GameOverText") as! SKLabelNode)
        gameOverNode.isHidden = true
        
        scoreNode = (self.childNode(withName: "Score") as! SKLabelNode)
        
        coinNode = (self.childNode(withName: "Coin") as! SKSpriteNode)
        
        coinSound = SKAction.playSoundFileNamed("coin", waitForCompletion: false)
        explosionSound = SKAction.playSoundFileNamed("explosion", waitForCompletion: false)
        
    }
    
    func addScore() {
        nCoins += 1
        scoreNode.text = String(nCoins)
        
        let randX = arc4random_uniform(UInt32(self.frame.width))
        let randY = arc4random_uniform(UInt32(self.frame.height))
        
        coinNode.position.x = CGFloat(randX) - self.frame.width / 2.0
        coinNode.position.y = CGFloat(randY) - self.frame.height / 2.0

        run(coinSound)

        let flipAnimation = SKAction.animate(with: coinFlipFrames, timePerFrame: 0.1)
        coinHUD.run(flipAnimation)
    }
    
    func gameOver() {
        playerNode.isHidden = true
        isGameOver = true
        gameOverNode.isHidden = false

        run(explosionSound)
    }
    
    func restartGame(){
        playerNode.isHidden = false
        isGameOver = false
        gameOverNode.isHidden = true
        
        nCoins = 0
        scoreNode.text = String(nCoins)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if isGameOver {
            restartGame()
        }
        
        let touch = touches.first //referência do primeiro toque
        let location = touch!.location(in: self) //posicão do primeiro toque
        playerNode.position = location //atualiza a posicão no Node

    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first //referência do primeiro toque
        let location = touch!.location(in: self) //posicão do primeiro toque
        playerNode.position = location //atualiza a posicão no Node
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        //Cálculo para correção de velocidade do inimigo, por conta dos quadros por segundo
        var timeSinceLastUpdate = 0.0
        if lastUpdateTime != 0 {
            timeSinceLastUpdate = currentTime - lastUpdateTime
        }
        lastUpdateTime = currentTime
        
        
        enemy1Node.position.x += enemy1Velocity.x * CGFloat(timeSinceLastUpdate)
        enemy1Node.position.y += enemy1Velocity.y * CGFloat(timeSinceLastUpdate)
        
        if enemy1Node.position.x < -(scene?.frame.width)!/2.0 {
            enemy1Node.position.x = -(scene?.frame.width)!/2.0
            enemy1Velocity.x *= -1
        }
        
        if enemy1Node.position.x > (scene?.frame.width)!/2.0 {
            enemy1Node.position.x = (scene?.frame.width)!/2.0
            enemy1Velocity.x *= -1
        }
        
        if enemy1Node.position.y < -(scene?.frame.height)!/2.0 {
            enemy1Node.position.y = -(scene?.frame.height)!/2.0
            enemy1Velocity.y *= -1
        }
        
        if enemy1Node.position.y > (scene?.frame.height)!/2.0 {
            enemy1Node.position.y = (scene?.frame.height)!/2.0
            enemy1Velocity.y *= -1
        }
        
        
        if playerNode.intersects(enemy1Node) {
            if !playerNode.isHidden {
                gameOver()
            }
        }
        
        if !isGameOver {
            if playerNode.intersects(coinNode) {
                addScore()
            }
        }
        
    }
    
    
}
